package montp.services;

import montp.data.dao.PersonDAO;
import montp.data.model.Person;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PersonService extends GenericService<Person, PersonDAO> {
    @Inject
    private UserService userService;

    @Override
    public void insert(Person instance) {
        userService.insert(instance);
    }
}
