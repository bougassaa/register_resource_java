package montp.services;

import montp.data.dao.ResourceTypeDAO;
import montp.data.model.ResourceType;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourceTypeService extends GenericService<ResourceType, ResourceTypeDAO> {
    @Override
    public boolean canDelete(ResourceType instance) {
        return instance.isNew() || instance.getResources().isEmpty();
    }
}
