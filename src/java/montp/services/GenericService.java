package montp.services;

import montp.data.dao.GenericDAO;
import montp.data.dao.ResultListMeta;
import montp.data.model.GenericEntity;
import montp.data.model.Resource;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

public class GenericService<T extends GenericEntity, DAO extends GenericDAO<T>> {

    @Inject
    protected DAO dao;

    public T get(long id) {
        return dao.get(id);
    }

    @Transactional
    public void insert(T instance) {
        dao.insert(instance);
    }

    @Transactional
    public void update(T instance) {
        dao.update(instance);
    }

    @Transactional
    public void delete(T instance) {
        dao.delete(instance);
    }

    public boolean canDelete(T instance) {
        return true;
    }

    public List<T> getAll() {
        return dao.getAll();
    }

    public Long rowCount() {
        return dao.rowCount();
    }

    public List<T> getResultListByMeta(ResultListMeta resultListMeta) {
        return dao.getResultListByMeta(resultListMeta);
    }

    public Long rowCountByMeta(ResultListMeta meta) {
        return dao.rowCountByMeta(meta);
    }
}
