package montp.services;

import montp.data.dao.ResourceDAO;
import montp.data.model.Resource;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourceService extends GenericService<Resource, ResourceDAO> {
}
