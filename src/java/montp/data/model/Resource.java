package montp.data.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Resource extends GenericEntity {

    @Column(nullable = false, length = 100)
    private String name;
    private Integer capacity = 0;
    private Boolean shareable = false;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Person manager;

    @ManyToOne
    @JoinColumn(nullable = false)
    private ResourceType type;

    @OneToMany(mappedBy = "resource")
    private List<Reservation> reservations;

    public Resource() {}

    // copy constructor
    public Resource(Resource object) {
        this.name = object.name;
        this.capacity = object.capacity;
        this.shareable = object.shareable;
        this.manager = object.manager;
        this.type = object.type;
        this.reservations = object.reservations;
    }

    public Resource(String name, Person manager, ResourceType type) {
        this.name = name;
        this.manager = manager;
        this.type = type;
    }

    public Resource(String name, Person manager, ResourceType type, Integer capacity, Boolean shareable) {
        this.name = name;
        this.capacity = capacity;
        this.shareable = shareable;
        this.manager = manager;
        this.type = type;
    }

    public boolean fieldsEqual(Resource r) {
        return r.name.equals(name) &&
                r.capacity.equals(capacity) &&
                r.shareable == shareable &&
                Objects.equals(r.manager.getId(), manager.id) &&
                Objects.equals(r.type.getId(), type.id);
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public Boolean getShareable() {
        return shareable;
    }

    public void setShareable(Boolean shareable) {
        this.shareable = shareable;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Person getManager() {
        return manager;
    }

    public void setManager(Person manager) {
        this.manager = manager;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
