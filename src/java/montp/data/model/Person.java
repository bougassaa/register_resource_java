package montp.data.model;

import montp.data.model.security.User;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Person extends User {
    private Boolean isManager = false;

    @OneToMany(mappedBy = "manager")
    private List<Resource> resources;

    @OneToMany(mappedBy = "person")
    private List<Reservation> reservations;

    public Person() {}

    public Person(String userName, String password) {
        super(userName, password);
    }

    public Person(String userName, String password, Boolean isManager) {
        super(userName, password);
        this.isManager = isManager;
    }

    public Boolean getIsManager() {
        return isManager;
    }

    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
