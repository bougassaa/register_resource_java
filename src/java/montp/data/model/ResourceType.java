package montp.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class ResourceType extends GenericEntity {

    @Column(nullable = false)
    private String name;
    private Boolean hasCapacity = false;
    private Boolean isShareable = false;
    @OneToMany(mappedBy = "type")
    private List<Resource> resources;

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public ResourceType() {}

    public ResourceType(String name) {
        this.name = name;
    }

    public ResourceType(String name, Boolean hasCapacity, Boolean isShareable) {
        this.name = name;
        this.hasCapacity = hasCapacity;
        this.isShareable = isShareable;
    }

    public Boolean getIsShareable() {
        return isShareable;
    }

    public void setIsShareable(Boolean isShareable) {
        this.isShareable = isShareable;
    }

    public Boolean getHasCapacity() {
        return hasCapacity;
    }

    public void setHasCapacity(Boolean hasCapacity) {
        this.hasCapacity = hasCapacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
