package montp.data;

import montp.data.model.Person;
import montp.data.model.Resource;
import montp.data.model.ResourceType;
import montp.data.model.security.Group;
import montp.data.model.security.User;
import montp.services.PersonService;
import montp.services.ResourceService;
import montp.services.ResourceTypeService;
import montp.services.UserService;
import montp.tools.Tools;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Startup
public class Seeder {
    
    @Inject
    private UserService userService;
    @Inject
    private ResourceTypeService resourceTypeService;
    @Inject
    private PersonService personService;
    @Inject
    private ResourceService resourceService;

    @PersistenceContext
    private EntityManager em;
    
    @PostConstruct
    public void init() {
        if (userService.getGroup("USER") == null) {
            Group groupUser = new Group("USER");
            em.persist(groupUser);
            Group groupAdmin = new Group("ADMIN");
            em.persist(groupAdmin);

            User userUser1 = new User("user1", "user1");
            List<Group> groupes = new ArrayList<>();
            groupes.add(groupUser);
            userUser1.setGroups(groupes);
            userService.insert(userUser1);

            User userAdmin = new User("admin", "admin");
            groupes.add(groupAdmin);
            userAdmin.setGroups(groupes);
            userService.insert(userAdmin);

            List<ResourceType> rts = new ArrayList<>();

            rts.add(new ResourceType("Véhicule"));
            rts.add(new ResourceType("Espace immobilier", true, true));
            rts.add(new ResourceType("Équipement"));
            rts.add(new ResourceType("Personnel"));

            resourceTypeService.insert(new ResourceType("Type sans ressource"));

            for (ResourceType r : rts) {
                resourceTypeService.insert(r);
            }

            List<Person> ps = new ArrayList<>();

            ps.add(new Person("manager1", "manager1", true));
            ps.add(new Person("manager2", "manager2", true));
            ps.add(new Person("person1", "person1"));
            ps.add(new Person("person2", "person2"));

            for (Person p : ps) {
                personService.insert(p);
            }

            for (int i = 1; i <= 40; i++) {
                // get random resource type
                ResourceType rt = rts.get(Tools.getRandomNumber(0, rts.size() - 1));
                // get random person
                Person p = ps.get(Tools.getRandomNumber(0, ps.size() - 1));

                Resource res = new Resource("Ressource Généré N°" + i, p, rt);

                // has capacity by random check
                if (Tools.getRandomNumber(0, 2) == 1) {
                    res.setCapacity(Tools.getRandomNumber(15, 35));
                    // is shareable by random check
                    if (Tools.getRandomNumber(0, 1) == 1) {
                        res.setShareable(true);
                    }
                }

                resourceService.insert(res);
            }
        }
    }

}
