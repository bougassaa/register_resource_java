package montp.data.dao;

import montp.data.model.Person;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PersonDAO extends GenericDAO<Person>{
    public PersonDAO() {
        super(Person.class);
    }
}
