package montp.data.dao;

import montp.data.model.ResourceType;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourceTypeDAO extends GenericDAO<ResourceType>{
    public ResourceTypeDAO() {
        super(ResourceType.class);
    }
}
