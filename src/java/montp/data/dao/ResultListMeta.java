package montp.data.dao;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.SortMeta;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResultListMeta {
    private int offset = -1;
    private int limit = -1;
    private Map<String, SortMeta> sort;
    private Map<String, FilterMeta> filter;

    private static final String QUERY_COUNT = "count";
    private static final String QUERY_SELECT = "select";

    public ResultListMeta() {
    }

    public ResultListMeta(int limit) {
        this.limit = limit;
    }

    public ResultListMeta(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public ResultListMeta(int offset, int limit, Map<String, SortMeta> sort, Map<String, FilterMeta> filter) {
        this.offset = offset;
        this.limit = limit;
        this.sort = sort;
        this.filter = filter;
    }

    public boolean hasPagination() {
        return offset != -1 && limit != -1;
    }
    
    public boolean hasSort() {
        return sort != null && !sort.isEmpty();
    }
    
    public boolean hasFilter() {
        return filter != null && !filter.isEmpty();
    }

    public String getFilterQuery() {
        String query = "";

        if (hasFilter()) {
            List<String> queries = new ArrayList<>();
            for (FilterMeta filter : filter.values()) {
                String field = filter.getField();
                Object value = filter.getFilterValue();

                queries.add(
                        // todo : make it generic for operators ?
                        String.format(" t.%s LIKE '%%%s%%' ", field, value)
                );
            }

            query = "WHERE " + String.join(" AND ", queries);
        }

        return query;
    }

    public String getSortQuery() {
        String query = "";
        if (hasSort()) {
            List<String> queries = new ArrayList<>();
            for (SortMeta sort : sort.values()) {
                queries.add(
                    String.format(" t.%s %s ", sort.getField(), sort.getOrder().isAscending() ? "asc" : "desc")
                );
            }

            query = "ORDER BY " + String.join(", ", queries);
        }

        return query;
    }

    public Query getSelectQuery(String type, EntityManager manager) {
        return getQuery(QUERY_SELECT, type, manager);
    }

    public Query getCountQuery(String type, EntityManager manager) {
        return getQuery(QUERY_COUNT, type, manager);
    }

    private Query getQuery(String queryType, String type, EntityManager manager) {
        Query q = manager.createQuery(
                String.format("SELECT %s FROM %s t %s %s",
                        (queryType.equals(QUERY_SELECT) ? "t" : "COUNT(t)"),
                        type,
                        getFilterQuery(),
                        getSortQuery()
                )
        );

        if (hasPagination() && queryType.equals(QUERY_SELECT)) {
            q.setFirstResult(offset)
             .setMaxResults(limit);
        }

        return q;
    }
}
