package montp.data.dao;

import montp.data.model.Resource;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourceDAO extends GenericDAO<Resource> {
    public ResourceDAO() {
        super(Resource.class);
    }
}
