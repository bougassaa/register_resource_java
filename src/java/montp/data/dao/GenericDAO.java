package montp.data.dao;

import montp.data.model.GenericEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

public abstract class GenericDAO<T extends GenericEntity> {

    @PersistenceContext
    protected EntityManager em;
    private Class<T> instanceClass;

    public GenericDAO() {
    }

    public GenericDAO(Class<T> instanceClass) {
        this.instanceClass = instanceClass;
    }

    public T get(long id) {
        return em.find(instanceClass, id);
    }

    @Transactional
    public void insert(T instance) {
        em.persist(instance);
        em.flush();
    }

    @Transactional
    public void update(T instance) {
        em.merge(instance);
    }

    @Transactional
    public void delete(T instance) {
        em.remove(em.find(instanceClass, instance.getId()));
    }

    public List<T> getAll() {
        return em.createQuery(String.format("SELECT t FROM %s t", instanceClass.getName()))
                .getResultList();
    }

    public Long rowCount() {
        return (Long) em.createQuery(String.format("SELECT COUNT(t) FROM %s t", instanceClass.getName()))
                .getSingleResult();
    }

    public List<T> getResultListByMeta(ResultListMeta meta) {
        return meta.getSelectQuery(instanceClass.getName(), em)
                .getResultList();
    }

    public Long rowCountByMeta(ResultListMeta meta) {
        return (Long) meta.getCountQuery(instanceClass.getName(), em)
                .getSingleResult();
    }
}
