package montp.web.controllers.resourceType;

import montp.data.dao.ResourceTypeDAO;
import montp.data.model.ResourceType;
import montp.services.ResourceTypeService;
import montp.web.controllers.abstraction.AbstractFormDialog;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("resourceTypeForm")
public class ResourceTypeFormDialog extends AbstractFormDialog<ResourceType, ResourceTypeDAO, ResourceTypeService> {
    @Override
    public void initObject() {
        object = new ResourceType();
    }

    public void save() {
        if (object.isNew()) {
            listView.add(object);
        } else {
            listView.update(object);
        }
    }
}
