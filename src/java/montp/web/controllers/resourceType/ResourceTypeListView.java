package montp.web.controllers.resourceType;

import montp.data.dao.ResourceTypeDAO;
import montp.data.model.ResourceType;
import montp.services.ResourceTypeService;
import montp.web.controllers.abstraction.AbstractListView;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("resourceTypeList")
public class ResourceTypeListView extends AbstractListView<ResourceType, ResourceTypeDAO, ResourceTypeService> {

}
