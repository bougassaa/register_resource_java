package montp.web.controllers.resource;

import montp.data.dao.ResourceDAO;
import montp.data.model.Resource;
import montp.services.ResourceService;
import montp.web.controllers.abstraction.AbstractListView;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("resourceList")
public class ResourceListView extends AbstractListView<Resource, ResourceDAO, ResourceService> {
    private ResourceDataModel lazyModel;

    public ResourceDataModel getLazyModel() {
        return lazyModel;
    }

    @PostConstruct
    @Override
    public void init() {
        lazyModel = new ResourceDataModel(service);
    }
}
