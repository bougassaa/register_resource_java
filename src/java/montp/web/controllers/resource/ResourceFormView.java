package montp.web.controllers.resource;

import montp.data.dao.ResourceDAO;
import montp.data.model.Person;
import montp.data.model.Resource;
import montp.data.model.ResourceType;
import montp.services.PersonService;
import montp.services.ResourceService;
import montp.services.ResourceTypeService;
import montp.web.controllers.abstraction.AbstractFormView;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@ViewScoped
@Named("resourceForm")
public class ResourceFormView extends AbstractFormView<Resource, ResourceDAO, ResourceService> {

    @Inject
    protected PersonService personService;
    @Inject
    protected ResourceTypeService resourceTypeService;

    private Resource objectNonModified;

    @Override
    public void setObject(Resource object) {
        if (object == null) {
            object = new Resource();
        }

        super.setObject(object);

        // used to compare if object has been modified
        if (objectNonModified == null && !object.isNew()) {
            setObjectNonModified(object);
        }
    }

    public void setObjectNonModified(Resource object) {
        this.objectNonModified = new Resource(object);
    }

    public String getMode() {
        return object.isNew() ? "input" : "output";
    }

    public void save() {
        if (object.isNew()) {
            service.insert(object);
        } else {
            service.update(object);
        }

        setObjectNonModified(object);
    }

    @PostConstruct
    public void init() {
        setObject(null);
    }

    public boolean renderSaveButton() {
        return object.isNew() || !object.fieldsEqual(objectNonModified);
    }

    public boolean renderCapacity() {
        return object.getType() != null && object.getType().getHasCapacity();
    }

    public boolean renderShareable() {
        return object.getType() != null && object.getType().getIsShareable();
    }

    @Override
    public boolean canDelete(Resource object) {
        return super.canDelete(object) && !object.isNew();
    }

    public List<Person> getManagers() {
        return personService.getAll();
    }

    public List<ResourceType> getTypes() {
        return resourceTypeService.getAll();
    }

    public void delete(Resource object) {
        service.delete(object);
        this.redirect("resource/list");
    }
}
