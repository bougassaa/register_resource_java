package montp.web.controllers.resource;

import montp.data.dao.ResultListMeta;
import montp.data.model.Resource;
import montp.services.ResourceService;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

import java.util.List;
import java.util.Map;

public class ResourceDataModel extends LazyDataModel<Resource> {
    private final ResourceService service;

    public ResourceDataModel(ResourceService service) {
        this.service = service;
    }

    @Override
    public List<Resource> load(int first, int pageSize, Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy) {
        ResultListMeta meta = new ResultListMeta(first, pageSize, sortBy, filterBy);

        this.setRowCount(
                service.rowCountByMeta(meta).intValue()
        );

        return service.getResultListByMeta(meta);
    }
}
