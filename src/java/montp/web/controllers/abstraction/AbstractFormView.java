package montp.web.controllers.abstraction;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.services.GenericService;


public abstract class AbstractFormView<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> extends AbstractView<Entity, Dao, Service> {

    protected Entity object;

    public Entity getObject() {
        return object;
    }

    public void setObject(Entity object) {
        this.object = object;
    }

}
