package montp.web.controllers.abstraction;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.locale.Messages;
import montp.services.GenericService;
import montp.web.FacesTools;

import javax.inject.Inject;
import java.io.Serializable;

public abstract class AbstractView<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> implements Serializable {

    @Inject protected Service service;
    @Inject protected Messages messages;

    public boolean canDelete(Entity object) {
        return service.canDelete(object);
    }

    public void redirect(String page) {
        FacesTools.redirect(page);
    }
}
