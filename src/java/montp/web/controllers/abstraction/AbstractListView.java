package montp.web.controllers.abstraction;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.services.GenericService;

import javax.annotation.PostConstruct;
import java.util.List;

public abstract class AbstractListView<
        Entity extends GenericEntity, 
        Dao extends GenericDAO<Entity>, 
        Service extends GenericService<Entity, Dao>> extends AbstractView<Entity, Dao, Service> {

    protected List<Entity> data;

    public List<Entity> getData() {
        return data;
    }

    public void setData(List<Entity> data) {
        this.data = data;
    }

    public void delete(Entity object) {
        try {
            service.delete(object);
            data.remove(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void add(Entity object) {
        try {
            service.insert(object);
            data.add(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update(Entity object) {
        try {
            service.update(object);
            data.set(data.indexOf(object), object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostConstruct
    public void init() {
        data = service.getAll();
    }
}
