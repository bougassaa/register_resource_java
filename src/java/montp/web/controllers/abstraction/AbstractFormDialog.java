package montp.web.controllers.abstraction;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.services.GenericService;

import java.io.Serializable;

public abstract class AbstractFormDialog<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> implements Serializable {

    protected Entity object;
    protected AbstractListView<Entity, Dao, Service> listView;

    public Entity getObject() {
        return object;
    }

    public void setObject(Entity object) {
        this.object = object;
    }

    // todo : replace by generic
    public abstract void initObject();

    public void initDialog(AbstractListView<Entity, Dao, Service> abstractListView) {
        this.listView = abstractListView;
        this.initObject();
    }

    public void initDialog(AbstractListView<Entity, Dao, Service> abstractListView, Entity object) {
        this.listView = abstractListView;
        this.object = object;
    }
}
