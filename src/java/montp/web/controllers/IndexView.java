package montp.web.controllers;

import montp.data.dao.UserDAO;
import montp.data.model.security.User;
import montp.services.UserService;
import montp.tools.EMailer;
import montp.web.FacesTools;
import montp.web.UserSession;
import montp.web.controllers.abstraction.AbstractListView;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;

@ViewScoped
@Named("index")
public class IndexView extends AbstractListView<User, UserDAO, UserService> {

    @Inject private UserSession session;
    @Inject private EMailer eMailer;

    private String emailTo;

    public String getHello() {
        return String.format(messages.get("example.hello"), session.getUser());
    }

    public boolean isUserActive(User user) { return service.isActive(user); }

    public void sendMail() {
        if (!emailTo.strip().isEmpty()) {
            try {
                eMailer.send(emailTo, messages.get("example.mailsubject"), messages.get("example.mailcontent"));
                FacesTools.addMessage(FacesMessage.SEVERITY_INFO, messages.get("example.mailsent"));
            } catch (MessagingException e) {
                FacesTools.addMessage(FacesMessage.SEVERITY_ERROR, messages.get("example.mailerror"), e.getMessage());
            }
        }
    }

    public String getEmailTo() { return emailTo;   }
    public void setEmailTo(String emailTo) {  this.emailTo = emailTo;  }
}
