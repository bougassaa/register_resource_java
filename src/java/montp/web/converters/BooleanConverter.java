package montp.web.converters;

import montp.locale.Messages;
import montp.tools.Tools;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Boolean.class)
public class BooleanConverter implements Converter<Boolean> {

    private final Messages messages;

    public BooleanConverter() {
        this.messages = Tools.lookupBean(Messages.class);
    }

    @Override
    public Boolean getAsObject(FacesContext context, UIComponent component, String value) {
        return value.equals(messages.get("app.yes")) || value.equals("true");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Boolean value) {
        return value ? messages.get("app.yes") : messages.get("app.no");
    }
}
